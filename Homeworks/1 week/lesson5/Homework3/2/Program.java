import java.util.Scanner;

public class Program{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность массива");
        int length = scanner.nextInt();
        int [] array = new int [length];
        int quantity = 0;
        int sum = 0;
        System.out.println("Введите элементы массива");
        for (int i = 0; i < array.length ; i++) {
            array[i] = scanner.nextInt();
            quantity ++;
            sum += array[i];
        }
        System.out.println(sum/quantity );
    }
}