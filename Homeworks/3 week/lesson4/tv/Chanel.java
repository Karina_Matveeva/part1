package tv;

public class Chanel {


    private char nameChanel[];
    private Program listOfProgram[];
    private TV tv;

    //мы должны описать вместимость нашего канала, т.е. сколько программ он содержит
    private final int MAX_QUANTITY_PROGRAM = 3;//кол-во программ

    //мы должны объявить переменную, которая будет хранить порядковый номер передачи на канале
    private int serialNumberProgram;//программа, которую мы выбираем


    Chanel(char nameChanel[]){
        this.nameChanel = nameChanel;
        //объявляем массив программ, размер которого равен нашей переменной
        this.listOfProgram = new Program[MAX_QUANTITY_PROGRAM];
        this.serialNumberProgram = 0;
    }

    //метод, добавляющий передачу в массив передач
    // Кладём каждую передачу в первое мустое место в массиве передач
    //первую передачу кладём в [0], вторую - [1] ...
    void placeInChanel(Program program){
        if (serialNumberProgram < MAX_QUANTITY_PROGRAM){
            System.out.println(this.listOfProgram[serialNumberProgram]= program);
            //у программы появляется свой канал, т.е. создается объектная переменная
            //которая указывает на тот канал, из которого вызван данный метод
            program.setChanel(this);
            this.serialNumberProgram++;

        }



    }



    void setTv(TV tv){
        this.tv = tv;
    }

}



//    private Program listOfProgram[];
//
//    private int serialNumberProgram;


//    Chanel(TV serialNumberChanel, char nameChanel[]){
//        this.serialNumberChanel = serialNumberChanel;
//        this.nameChanel=nameChanel;
//        this.listOfProgram = new Program[MAX_QUANTITY_PROGRAM];
//        serialNumberProgram= 0;
//
//    }
//
//
////передачи
//   void setListOfShows(Program nameProgram){
//       if(serialNumberProgram < MAX_QUANTITY_PROGRAM){
//           this.listOfProgram[serialNumberProgram] = nameProgram;
//           nameProgram.setChanel(this);
//           this.serialNumberProgram ++;
//       }
//   }
//}
