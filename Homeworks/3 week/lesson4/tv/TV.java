package tv;

public class TV {

    private RemoteControl remoteControl;

    private char nameTv[];
    private Chanel listOfChanel[];//массив с каналами в телеке

    //мы должны описать вместимость нашего телека, т.е. сколько каналов он содержит
    private final int MAX_QUANTITY_CHANEL = 5;

    //мы должны объявить переменную, которая будет хранить порядковый номер канала на телеке
    private int serialNumberChanel;

    //опишем конструктор
    TV(char nameTv[]){
        this.nameTv = nameTv;
        //объявили пустой массив каналов
        this.listOfChanel = new Chanel[MAX_QUANTITY_CHANEL];
        this.serialNumberChanel = 0;
    }

    //метод, добавляющий канал в массив. Каждый канал кладем в первое пустое место в массиве
    //первый канал кладём в [0], второй - [1] ...
    void placeInTv(Chanel chanel){
        //проверка, не превышает ли номер канала кол-во каналов в телеке
        if (serialNumberChanel < MAX_QUANTITY_CHANEL){
            //если оно меньше, то мы добавляем наш канал в массив телека
            //массив каналов под текущим номером = ссылке на определенный канал
            System.out.println(this.listOfChanel[serialNumberChanel] = chanel);
            //у канала появляется свой телек, т.е. создается объектная переменная
            //которая указывает на тот телек, из которого вызван данный метод
            chanel.setTv(this);//теперь канал имеет ссылку на автобус, а автобус имеет канал из массива всех каналов
            serialNumberChanel++;



        }

    }

    public void setRemoteControl(RemoteControl remoteControl) {
        this.remoteControl = remoteControl;
    }

    public int getSerialNumberChanel() {
        return serialNumberChanel;
    }

    public void setSerialNumberChanel(int serialNumberChanel) {
        this.serialNumberChanel = serialNumberChanel;
    }

    //    private Chanel listOfChanel[];
//    private int serialNumberChanel;


//    void setRemoteControl(RemoteControl remoteControl){
//        this.remoteControl = remoteControl;
//    }
//
//    TV(char nameTv[]){
//        this.nameTv = nameTv;
//        this.listOfChanel = new Chanel[MAX_QUANTITY_CHANEL];
//        this.serialNumberChanel = 0;
//
//    }
//    //метод, чтобы выбрать канал
//    void selectoinChanel(Chanel nameChanel){
//        if(serialNumberChanel < MAX_QUANTITY_CHANEL){
//            this.listOfChanel[serialNumberChanel] = nameChanel;
//            serialNumberChanel++;
//
//
//        }
//    }


}
