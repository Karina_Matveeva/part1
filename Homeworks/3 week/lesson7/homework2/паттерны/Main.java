package паттерны;

public class Main {
    public static void main(String[] args) {
        User myUser1 = new User.Builder()
                .withFistName("Арина")
                .withLastName("Скорнякова")
                .withAge(8)
                .build();
        User myUser2 = new User.Builder()
                .withFistName("Лариса")
                .withAddress("улица Камская 1")
                .withAge(9)
                .build();
        User myUser3 = new User.Builder()
                .withLastName("Матвеева")
                .withAddress("улица Сосновая 27")
                .build();


    }
}
