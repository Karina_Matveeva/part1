package паттерны;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private String address;

    public static class Builder{
        private User newUser;

        public Builder(){//конструктор
            newUser = new User();
        }

        //параметры
        public Builder withFistName(String firstName){
            newUser.firstName = firstName;
            return this;
        }

        public Builder withLastName(String lastName){
            newUser.lastName = lastName;
            return this;
        }

        public Builder withAge(int age){
            newUser.age = age;
            return this;
        }

        public Builder withAddress(String address){
            newUser.address = address;
            return this;
        }

        //метод, который возвращает готовый объект
        public User build(){
            return newUser;
        }

////        public Builder(User myUser){
//            this.build(myUser);
//        }
    }

}
