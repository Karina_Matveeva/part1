package Вложенныеклассы;

import Lists.Node;

public class MainForIntegersArrayList {
    public static void main(String[] args) {
        //реализация связного списка, который хранит в себе числа

        IntegerArrayList list = new IntegerArrayList();

        list.add(7);
        list.add(12);
        list.add(16);
        list.add(20);
        list.add(25);

        //вывод эл-тов списка
//        for (int i = 0; i <list.getCount() ; i++) {
//            System.out.println(list.get(i));
//
//        }

        //использование метода гет не самый лучший вариант, поэтому здесь нужно приметь Паттер - "итератор"
        //объявляем наш внешний класс и обращаемся к вложенному классу,который не статический!!!! здесь ситуация уже немного другая
        //мы должна обратиться к уже созданному объекту IntegersArrayList
        IntegerArrayList.ArrayListIterator iterator = list.new ArrayListIterator();
        //таким образом мы создали экземляр итератора через объект IntegerArrayList list, т.е. у на сне статический вложенный класс, то создания объекта
        //такого класса возможно только через уже имеющийся объект внешнего класса

        while (iterator.hasNext() ){
            System.out.println(iterator.next());
        }

    }
}
