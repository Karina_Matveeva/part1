package Вложенныеклассы;

public class IntegerArrayList {
    private int [] elements;
    private static final int DEFAULT_ARRAY_SIZE = 10;//константа, хранящая значение 10
    private int count;


    //воспользуемся вложенным не статическим внутренним классом для создания ИТЕРАТОРА
    //особенность не статического вложенного класса заключается в том, что он всегда имеет доступ к полям внешнего класса

    class ArrayListIterator{ // итератор - то самое шаблонное решение, которое позволяет проходить или итерировать по списку по очереди
        int current;//индекс элемента, который мы сейчас просматриваем
        /* по своей сути итератор -  какой-то укзаатель, который всегда стоит перед каким-либо элементом. То есть, если это первый элемент,
        то мы стоим перед ним, и соостветсвенно вызвав некст мы получаем слудеющий после этого указателя, то есть наш самый первый элемент
         */

        ArrayListIterator(){
            this.current = 0;
        }
        boolean hasNext(){
            //есть ли следующий ?
            return current < count;//current - индекс текущего элемента, count - индекс последнего эл-та
        }

        int next(){ //метод, который будет возвращать следующий элемент
            int nextElement = elements[current];
            current++;
            return nextElement;
        }
        /*
        таким образом мы сделали внутренний класс, который имеет доступ к членам внешнего класса, в отличие от класса Node
         */
    }

    //конструкторы делаю public, дабы они были открыты для доступа
    public IntegerArrayList(){
        this.elements = new int[DEFAULT_ARRAY_SIZE];//инициализируем массив размером нашей константы
        this.count = 0;//кол-во элементов в массиве
    }

    public void add(int element){
        if (count < this.elements.length){
            this.elements[count] = element;
            count++;
        }else {
            System.out.println("array out of bounds");
        }
        //мы всегда добавляем новый элемент в первый пустой элемент списка, count отсчитывает кол-во элементов в списке, соотвественно поскольку элементы
        // отсчитываются с нуля, то count всегда будет содеражать индекс первого пустого элемента в массиве
    }


    public int get(int index){
        //функция, которая проверяет, чтобы индекс был меньше, чем count. То есть мы не можем запросить элемент, которого еще впринцепи нет.
        if (index >= 0 && index < count){
            return elements[index];
        }else{
            System.out.println("error");
            return -1;
        }
    }

    public int getCount() {
        return count;
    }
}
