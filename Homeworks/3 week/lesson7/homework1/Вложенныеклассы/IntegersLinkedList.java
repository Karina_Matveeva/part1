package Вложенныеклассы;

public class IntegersLinkedList {

    //как хранить наше первое поле? Создаем:
    private Node first;//создали первый узел нашего списка
    private int count;//указывает на то, сколько текущих элементов есть
    private Node last;//хранить указатель на последний эл-т


    public IntegersLinkedList() {
        this.count = 0;
    }

     class Node { // в данном случае класс Node будет статически вложен во внешний класс IntegersLinkedList
         //сделали данный класс приватным, т.к. никто о нём ничего не должен знать
        /*какие же особенности у статических вложенных классво?
        внутри данного класса мы не можем обращаться к полям внешнего класса, т.к. он статический, а внешний не статический!
         */
         private int value;
         private Node next;

         //поэтому теперь нам не нужны геттеры и сеттеры, поскольку эти методы в люом слуае будут видны внутри IntegersLinkedList
         //данный класс имеет отношение только ко внешнему классу IntegersLinkedList и позволяет сосредоточить определнную логику одной сущности
         //внутри определенной логики другой сущности

         Node(int value) {
             this.value = value;
         }

         public int getValue() {
             return value;
         }
     }

     public class LinkedListIterator{
         private Node current;
          LinkedListIterator(){
              this.current = first;
          }


         boolean hasNext() {
             //есть ли следующий?
             return current != null;
         }

         int next() {
              //метод, который будет возвращать следующий элемент
             int tmp = current.getValue();
             current = current.next;
             return tmp;

         }
     }



    /*
    особенность нестатического вложенного клссса в том, что он всегда имеет доступ к полям внешнего класса

     */







    public void add(int element){
//добавляем элементы в конец спика, следовательно
        Node newNode = new Node(element);
        if(first == null){
//если первый элемент = о, то сразу новый узел становится первым
            this.first = newNode;
            this.last = newNode;

        }else {
            this.last.next = newNode;//следующий после последнего - новый узел
            this.last = newNode;//но и сам последний - новый узел
        }
        this.count++;

    }



}
