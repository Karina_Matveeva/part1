package Lists;



public class IntegersLinkedList {

    //как хранить наше первое поле? Создаем:
    private Node first;//создали первый узел нашего списка
    private int count;//указывает на то, сколько текущих элементов есть
    private Node last;//хранить указатель на последний эл-т



    public IntegersLinkedList(){
        this.count = 0;
    }

    public void add(int element){
        //добавляем элементы в конец спика, следовательно
        Node newNode = new Node(element);
        if(first == null){
            //если первый элемент = о, то сразу новый узел становится первым
            this.first = newNode;
            this.last = newNode;
        }else {
//            //в противном же случае нам нужно дойти до узла, у кого ссылка на следующий
//            //узел будет равна null
//            Node current = first;
//            while (current.getNext() != null){//доходим до этого узла, т.е.
//                //пока следующий элемент после current не равен нулю, мы начинаем
//                //двигать наш current. Мы проходим по всему связному списку и
//                //останавливаемся на последнем элементе
//                current = current.getNext();
//            }
//            //и уже у последнего элемента появляется ссылка на новый узел.
//            //Такиим образом мы добавляем новый элемент в конец списка
//            current.setNext(newNode);
            //*******************другой УДОБНЫЙ способ****************
            this.last.setNext(newNode);//следующий после последнего - новый узел
            this.last = newNode;//но и сам последний - новый узел
        }
        this.count++;

    }

    public int get(int index){
        //т.к. в связном списке нет индексации, как в массиве, то
        //необходимо создать доп цикл, который будет отсчитывать кол-во
        // элементов  до этого индекса

            Node current = first;
            for (int i = 0; i < index; i++) {
                current = current.getNext();
            }
            return current.getValue();
    }

    public void add(int element, int index){

        Node current  = first;
        for (int i = 0; i < index - 1; i++) {
            current = current.getNext();
        }
        Node newNode = new Node(element);
        if( current == null){
            first = newNode;
            last = newNode;

        }else if(current == last){
            last.setNext(newNode);
            last = last.getNext();
        }else {
            newNode.setNext(current.getNext());
            current.setNext(newNode);
        }

    }

    public void delete(int index){
        Node current  = first;
        for (int i = 0; i < index - 1; i++) {
            current = current.getNext();
        }
        Node current2 = current.getNext();//создала новый узел, куда
        //кинула элемент перед тем, который нужно удалить
        current.setNext(current2.getNext());
        current2.setNext(current);
    }

    public void reverse(){
        Node newNode0 = first.getNext();
        first.setNext(null);
        Node newNode1 = newNode0.getNext();
        newNode0.setNext(first);

        do {

            first = newNode0;
            newNode0 = newNode1;
            newNode1 = newNode0.getNext();
            newNode0.setNext(first);
        }while (newNode1 != null);


    }

}


