package Lists;

public class MainForIntegersArrayList {
    public static void main(String[] args) {
        //реализация связного списка, который хранит в себе числа

        Node a = new Node(10);
        Node b = new Node(15);
        Node c = new Node(20);
        Node d = new Node(25);

        a.setNext(b);
        b.setNext(c);
        c.setNext(d);
        //данным способом мы сформировали связный список, первый узел которого хранит информацию о всех узлах нашего списка


        Node current = a;
        while (current != null){
            System.out.println(current.getValue());
            current = current.getNext();
        }
        //данным образом мы образовали БЕГУНОК, наша объектная переменная current тоже указывает на тот же самй узел, что и переменная а, и потом она с
        //смещается на узел
    }
}
