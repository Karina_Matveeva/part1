package Lists;


import java.util.Arrays;

public class IntegerArrayList {
    private int [] elements;
    private static final int DEFAULT_ARRAY_SIZE = 10;//константа, хранящая значение 10
    private int count;


    public void setElements(int[] elements) {
        this.elements = elements;
    }

    //конструкторы делаю public, дабы они были открыты для доступа
    public IntegerArrayList(){
        this.elements = new int[DEFAULT_ARRAY_SIZE];//инициализируем массив размером нашей константы
        this.count = 0;//кол-во элементов в массиве
    }

    public void add(int element){
        if (count < this.elements.length){
            this.elements[count] = element;
            count++;
        }else {
            System.out.println("array out of bounds");
        }
        //мы всегда добавляем новый элемент в первый пустой элемент списка, count отсчитывает кол-во элементов в списке, соотвественно поскольку элементы
        // отсчитываются с нуля, то count всегда будет содеражать индекс первого пустого элемента в массиве
    }


    public int get(int index){
        //функция, которая проверяет, чтобы индекс был меньше, чем count. То есть мы не можем запросить элемент, которого еще впринцепи нет.
        if (index >= 0 && index < count){
            return elements[index];
        }else{
            System.out.println("error");
            return -1;
        }
    }

    public void add(int element, int index) {
        if ( elements.length <= DEFAULT_ARRAY_SIZE ) {
//            int [] elementsCopy = Arrays.copyOf(elements, elements.length + 1);
            int [] elementsCopy = new int[elements.length + 1];
            for (int i = 0; i < elements.length ; i++) {
                elementsCopy[i] = elements[i];
            }

            for (int i = index; i < elementsCopy.length - 1; i++) {
                elementsCopy[i + 1] = elements[i];
            }
            elementsCopy[index] = element;
            setElements(elementsCopy);
        }
    }

    public void delete(int index){
        int [] elementsCopy = new int[elements.length - 1];
        for (int i = 0; i < elementsCopy.length ; i++) {
            elementsCopy[i] = elements[i];
        }
        for (int i = index; i < elementsCopy.length; i++) {
            elementsCopy[i] = elements[i + 1];
        }
        setElements(elementsCopy);
    }

    public  void  reverse( ){
        int [] list1 = new int[elements.length];
        for (int i = 0; i < elements.length ; i++) {
            list1[i] = elements[elements.length - i - 1];
        }
        setElements(list1);
    }

    public void fullness(){
        int temp = elements.length * 1/2;
        int [] elementsCopy = new int[temp];
        if ( elements.length == DEFAULT_ARRAY_SIZE){
            for (int i = 0; i < elementsCopy.length ; i++) {
                elementsCopy[i] = elements[i];
            }
        }

    }

    //посмотреть, что выводит
    @Override
    public String toString() {
        return "IntegerArrayList = " + Arrays.toString(elements);
    }

}
