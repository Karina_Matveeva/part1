import java.util.Scanner;

public class Program{


    public static double function(double x) {
        return x * x;
    }

    public static double integralSquare(double a, double b, double n) {
        double h = (b - a) / n;//расстояние между двумя точками в нашем разбиении

        double result = 0;
        for (double x = a; x <= b; x += h) {
            //рассчитаем площадь текущего прямоугольника
            double s = function(x) * h;
            result += s;
        }
        return result;
    }

    public static double integralSimpsona(double a, double b, int n) {

        double h = (b - a) / n;
        double sum = 0;

        for (double x = a; x <= b; x += 2 * h) {
            sum += function(x - h) + 4 * function(x) + function(x + h);
        }
        return sum;
    }


    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        System.out.println("введите нижнюю границу интегрирования :");
        double a = scanner.nextDouble();//нижняя граница интегрирования
        System.out.println("введите верхнюю границу интегрирования : ");
        double b = scanner.nextDouble();//верхняя граница интегрирования
        System.out.println("введите желаемое кол-во итераций: ");
        int n = scanner.nextInt();

        int nall = 130;//доп переменная для создания таблицы
        String minus = "-";

        for (int i = 0; i < nall; i++) {
            System.out.print(minus);
        }
        System.out.println();
        int n1 = 4;
        System.out.printf("%-22s", "НАЗВАНИЕ МЕТОДА");
        System.out.print("|");
        System.out.printf("%-20s", "КОЛИЧЕСТВО ИТЕРАЦИЙ");
        System.out.print("|");
        System.out.printf("%-53s", "РЕЗУЛЬТАТ ВЫЧИСЛЕНИЯ");
        System.out.println();
        for (int i = 0; i < nall; i++) {
            System.out.print(minus);
        }
        System.out.println();
        System.out.printf("%-22s", "МЕТОД ПРЯМОУГОЛЬНИКОВ");
        System.out.print("|");
        System.out.printf("%-20s", n);
        System.out.print("|");
        System.out.printf("%-53s", integralSquare(a, b, n));
        System.out.println();
        for (int i = 0; i < nall; i++) {
            System.out.print(minus);
        }
        System.out.println();
        System.out.printf("%-22s", "МЕТОД СИМПСОНА");
        System.out.print("|");
        System.out.printf("%-20s", n);
        System.out.print("|");
        System.out.printf("%-53s", integralSimpsona(a, b, n));

    }
}