import java.util.Scanner;

public class Program{

    public static int stringsCompare(char[] a, char[] b) {
        int n = 0;//доп переменная, в которой будем хранить длину массива
        if (a.length < b.length) {
            n = a.length;
        } else {
            n = b.length;
        }

        int i = 0;

        while (i < n) {
            if (a[i] < b[i]) {
                return -1;
            } else if (a[i] > b[i]) {
                return 1;
            }
            i++;
        }

        if (a.length == b.length) {
            return 0;
        } else if (a.length < b.length) {
            return -1;
        } else if (a.length > b.length) {
            return 1;
        }
        return i;

    }

    public static int transformation(char[] a) {
        int digit = 0;
        int result = 0;
        int temp = 1;
        for (int i = a.length - 1; i >= 0; i--) {
            digit = a[i] - '0';
            digit *= temp;
            temp *= 10;
            result += digit;
        }
        return result;
    }

    public static int repeatLetter(char[] str) {

        int array [] = new int[123];
        int i = 0;

            while (i < str.length){
                int current = str[i];
                array[current]++;
                i++;
            }

        //поиск максимума
        int maximum = array[97];
        int maxIndex = 97;

        for ( i = 97; i < array.length; i++) {
            if (maximum < array[i]) {
                maximum = array[i];
                maxIndex = i;
            }
        }
        return (char)maxIndex;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] a = {'a', 'a', 'c'};
        char[] b = {'a', 'b', 'b'};
        int result = stringsCompare(a, b);
        System.out.print(result);
        if (result == 0) {
            System.out.print(" = > строки совпадают ");
        }
        if (result == -1) {
            System.out.print(" = > строка а в лексикографическом порядке стоит раньше строки b");
        }
        if (result == 1) {
            System.out.print(" = >  строка а в лексикографическом порядке стоит позже строки b");
        }
        System.out.println();

        char[] digit = {'5', '6', '8'};
        System.out.print("полученное число : ");
        System.out.println(transformation(digit));

        System.out.println("Введите строку символов: ");
        char[] letter = scanner.nextLine().toCharArray();
        System.out.print("повторяющийся символ " +(char)repeatLetter(letter));
    }
}
